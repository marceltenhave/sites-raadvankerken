<?php
/**
 * Template Name: beginpagina
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
       <div id="metselwerk">
           <?php echo do_shortcode("[post_grid id='24441']"); ?>            
	   </div><!-- #metselwerk -->
	</div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php /**get_sidebar(); */ ?>
<?php get_footer(); ?>