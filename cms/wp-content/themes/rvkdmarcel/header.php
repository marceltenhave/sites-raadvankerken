<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>  
  	<meta charset="<?php bloginfo('charset'); ?>" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php wp_title('&#124;', true, 'right'); ?></title>
<link href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" rel="shortcut icon" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-26706007-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>    
    <?php wp_head(); ?>


  <body <?php body_class(); ?>>

 	<!-- logo and navigation -->

 <nav id="site-navigation" class="main-nav" role="navigation"> 
    <div id="main-nav-wrapper">    
                <div id="logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>"  title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
              
                    <?php $pinbin_options = get_option('theme_pinbin_options'); ?>

                <?php if ( $pinbin_options['logo'] != '' ): ?>
                  <div id="logo">
                    <img src="<?php echo $pinbin_options['logo']; ?>" />
                  </div>
                <?php  endif; ?>
              </a>
           
         </div>  
          <?php if ( has_nav_menu( 'main_nav' ) ) { ?>
          <?php wp_nav_menu( array( 'theme_location' => 'main_nav' ) ); ?>
          <?php } else { ?>
          <ul><?php wp_list_pages("depth=3&title_li=");  ?></ul>
          <?php } ?> 

   </div>

  </nav>      


<div class="clear"></div>
<div id="wrap"> 


<div id="header">
<div id="headerextra">
<div class="datumdelft">
<?php 
echo date_i18n('l j F Y'); 
?>
</div>
<div class="tekstdelft">
<?php
echo "Dagelijks overzicht van het beste nieuws uit kerkelijk Delft. Staat het hier niet, dan hoeft u het niet te weten." ;
?>
</div>
<div id="zoekmachine"> <?php get_search_form(); ?></div> 
</div>
</div>
  
  