<?php
/**
  * Template Name: beginpagina_3
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */

get_header(); ?>
     
        <div id="beginpagina" class="beginpaginainhoud" role="main">
               <?php echo do_shortcode("[post_grid id='24485']"); ?>
	    </div><!-- #beginpagina .beginpaginainhoud -->


<?php get_footer(); ?>