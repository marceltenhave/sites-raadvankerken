<?php
/**
 * Template Name: HomeSpeciaal
 *
 */
?>

<?php get_header(); ?>
<?php
$lcmp_page_head = <<<TXT
<div class="contentLayout">
<div class="sidebar1">
<?php include (TEMPLATEPATH . '/sidebar1.php'); ?>
</div>
<div class="content">

TXT;
$lcmp_is_left = strpos($lcmp_page_head,"sidebar");
if($lcmp_is_left===FALSE){
    $lcmp_rightdefault = 'default';
    $lcmp_leftdefault = 'notdefault';
}else{
    $lcmp_leftdefault = 'default';
    $lcmp_rightdefault = 'notdefault';
}
?>
<div class="contentLayout">
<?php 
global $lcmp_sidebarloc;
if($lcmp_sidebarloc == $lcmp_leftdefault || $lcmp_sidebarloc == 'left'){ ?>
<div class="sidebar1">
<?php include (TEMPLATEPATH . '/sidebar1.php'); ?>
</div>
<?php } ?>
<div class="content">

<div id="homevandaag"> 
<h1><?php echo ('Het Nieuws Van Vandaag:') ?></h1>
  <?php 
  function filter_where($where = '') { 
    // Posts in the last day 
    $where .= " AND post_date > '" . date('Y-m-d', strtotime('+2 hours')) . "'"; 
    return $where; 
  } 
add_filter('posts_where', 'filter_where'); 
?>    
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
               <div class="PostContent">
               <?php if (is_search()) the_excerpt(); else the_content(__('Lees méér &raquo;', 'kubrick')); ?>
               </div>
          <?php endwhile; endif; ?>
<?php remove_filter( 'posts_where', 'filter_where' );  ?>    
</div> 

<div id="homegisteren">
<h1><?php echo ('Het Nieuws Van Gisteren:') ?></h1>  
  <?php 
function filter_where2( $where = '' ) {
	// posts  6 to 2 days old
	$where .= " AND post_date >= '" . date('Y-m-d', strtotime('-22 hours')) . "'" . " AND post_date <= '" . date('Y-m-d', strtotime('+2 hours')) . "'";
	return $where;
}

add_filter( 'posts_where', 'filter_where2' ); 
?>    
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
               <div class="PostContent">
               <?php if (is_search()) the_excerpt(); else the_content(__('Lees méér &raquo;', 'kubrick')); ?>
               </div>
          <?php endwhile; endif; ?>
<?php remove_filter( 'posts_where', 'filter_where2' );  ?>    

</div>   

<div id="homeoudereberichten"> 
<h1><?php echo ('Het Nieuws Van Twee Dagen Geleden Of Ouder:') ?></h1> 
  <?php 
function filter_where3( $where = '' ) {
	// posts  7 to weetik veel days old
	$where .= " AND post_date >= '" . date('Y-m-d', strtotime('-100 days')) . "'" . " AND post_date <= '" . date('Y-m-d', strtotime('-22 hours')) . "'";
	return $where;
}

add_filter( 'posts_where', 'filter_where3' ); 
?>    
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
               <div class="PostContent">
               <?php if (is_search()) the_excerpt(); else the_content(__('Lees méér &raquo;', 'kubrick')); ?>
               </div>
          <?php endwhile; endif; ?>
<?php remove_filter( 'posts_where', 'filter_where3' );  ?>    

</div>
<div id="homearchief">
<h1><?php echo ('Hieronder vindt u het berichtenarchief.  Selecteer de maand van uw keuze.') ?></h1>
<select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'>
<option value=""><?php echo attribute_escape(__('Select Month')); ?></option>
<?php wp_get_archives('type=monthly&format=option&show_post_count=1'); ?> </select>
</div>
</div>
<?php 
global $lcmp_sidebarloc;
if($lcmp_sidebarloc == $lcmp_rightdefault || $lcmp_sidebarloc == 'right'){ ?>
<div class="sidebar1">
<?php include (TEMPLATEPATH . '/sidebar1.php'); ?>
</div>
<?php } ?>
<?php if(file_exists(TEMPLATEPATH . '/sidebar2.php')){ ?>
<div class="sidebar2">
<?php include (TEMPLATEPATH . '/sidebar2.php'); ?>
</div>
<?php } ?>
</div>
<div class="cleared"></div>
<?php get_footer(); ?>