<?php
/**
 * Template Name: SplashRVK
 *
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />


<div id="splashachtergrond">  
<div class="DatumSplash"> <?php echo date_i18n('l, j F Y'); ?></div>
<div class="SplashKoptekst">Raad van Kerken Delft</div>
     <div class="splashcontent">
<?php echo "<input type=\"button\" onClick=\"javascript:location.href='http://www.raadvankerkendelft.nl/home/';\" value=\"Naar de website\"/>"; ?>
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
               <div class="PostContent">
               <?php if (is_search()) the_excerpt(); else the_content(__('Lees méér &raquo;', 'kubrick')); ?>
               </div>
          <?php endwhile; endif; ?>
<?php echo "<input type=\"button\" onClick=\"javascript:location.href='http://www.raadvankerkendelft.nl/home/';\" value=\"Naar de website\"/>"; ?>
     </div>
</div>
<div id="splashklik">

  <img src="<?php bloginfo('template_directory'); ?>/images/Splash_leeg.png" alt="" width="1680" height="1050" border="0" usemap="#Map">

  <map name="Map"><area shape="rect" coords="0,0,1680,1050"  href="<?php echo site_url(); ?>/home" alt="Klik om de site te bezoeken"></Map>
</div>
