<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">

<label for="s"><?php _e('Zoek naar:', 'kubrick'); ?></label>

<div><input type="text" value="<?php the_search_query(); ?>" name="s" id="s" style="width: 98%;" />

<button class="Button" type="submit" name="search">
 <span class="btn">
  <span class="t"><?php _e('Zoek', 'kubrick'); ?></span>
  <span class="r"><span></span></span>
  <span class="l"></span>
 </span>
</button>
</div>
</form>

